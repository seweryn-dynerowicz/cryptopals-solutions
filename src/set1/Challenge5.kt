package set1

import kotlin.experimental.xor

class Challenge5 {
    companion object {
        fun repeatingXOR(bytes: ByteArray, key: ByteArray) : ByteArray =
            ByteArray(bytes.size).apply {
                var kIndex = 0
                bytes.forEachIndexed { index, byte ->
                    this[index] = byte.xor(key[kIndex])
                    kIndex = (kIndex + 1) % key.size
                }
            }

        fun repeatingXOR(input: String, key: String) : ByteArray =
            ByteArray(input.length).apply {
                val bytes = input.toByteArray()
                val kBytes = key.toByteArray()
                var kIndex = 0
                bytes.forEachIndexed { index, byte ->
                    this[index] = byte.xor(kBytes[kIndex])
                    kIndex = (kIndex + 1) % key.length
                }
            }

        @JvmStatic
        fun main(args: Array<String>) {
            val input = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"

            val key = "ICE"
            val encrypted = repeatingXOR(input, key)

            println("Encrypted : ${Challenge1.fromBytesToHexadecimals(encrypted)}")
        }
    }
}