package set1

import split
import java.io.File
import kotlin.experimental.xor
import kotlin.math.floor

class Challenge6 {
    companion object {
        fun hamming(byte1: Byte, byte2: Byte) : Int =
            byte1.xor(byte2).countOneBits()

        fun hamming(bytes1: ByteArray, bytes2: ByteArray) : Int {
            if(bytes1.size != bytes2.size)
                error("Size mismatch")

            var distance = 0
            for(index in bytes1.indices)
                distance += hamming(bytes1[index], bytes2[index])
            return distance
        }

        fun hamming(bytes: ByteArray, baseIndex1: Int, baseIndex2: Int, count: Int) : Int {
            if(baseIndex1 + count < bytes.size && baseIndex2 + count < bytes.size) {
                var distance = 0
                for(index in 0 until count)
                    distance += hamming(bytes[baseIndex1 + index], bytes[baseIndex2 + index])
                return distance
            } else
                error("Base indices cause overflow. ${bytes.size} with $baseIndex1 and $baseIndex2 +$count")
        }

        fun hamming(string1: String, string2: String) : Int {
            if(string1.length != string2.length)
                error("Size mismatch")

            return hamming(string1.toByteArray(), string2.toByteArray())
        }

        fun keySizeEstimation(bytes : ByteArray, lower: Int = 2, upper: Int = 40) : Map<Float, List<Int>> =
            HashMap<Float, MutableList<Int>>().apply {
                for(keysize in lower .. upper) {
                    val numberOfBlocks = floor(bytes.size.toFloat() / keysize).toInt()
                    val distances = FloatArray(numberOfBlocks-1)
                    for(block in 0 until numberOfBlocks - 2) {
                        distances[block] = hamming(bytes, baseIndex1 = block * keysize, baseIndex2 = (block + 1) * keysize, count = keysize).toFloat() / keysize.toFloat()
                    }
                    val distance = distances.sum() / (numberOfBlocks - 1)
                    this[distance] = (this[distance] ?: ArrayList()).apply { add(keysize) }
                }
            }.toList().sortedBy { (hamming, _) -> hamming }.toMap()

        @JvmStatic
        fun main(args: Array<String>) {
            // Decryption time !
            val string1 = "this is a test"
            val string2 = "wokka wokka!!!"

            println("Hamming distance : ${hamming(string1, string2)}")

            val string = StringBuilder().apply {
                File("challenge6-encrypted.txt").forEachLine { line ->
                    append(line)
                }
            }.toString()

            val bytes = Challenge1.fromBase64ToBytes(string)

            println("Bytes : ${bytes.size}")

            val keysizeHamming = keySizeEstimation(bytes, lower = 2, upper = 40)
            println("Hamming distances : keysizes")
            keysizeHamming.forEach { (distance, keysizes) ->
                println("Distance $distance : $keysizes")
                    keysizes.forEach { keySize ->
                        val parts = bytes.split(numberOfParts = keySize)
                        if(parts.all { part ->
                            val (_, minimalScore) = Challenge3.minimalScore(part)
                            minimalScore < Float.MAX_VALUE
                        }) {
                            val guessedKey = ByteArray(keySize)
                            parts.forEachIndexed { index, part ->
                                val (matchingKey, _) = Challenge3.minimalScore(part)
                                guessedKey[index] = matchingKey
                            }
                            println("Key guessed (size=$keySize) : ${Challenge1.fromBytesToCharacters(guessedKey)}")
                            println(Challenge1.fromBytesToCharacters(Challenge5.repeatingXOR(bytes, guessedKey)))
                            println()
                            println()
                        }
                    }
            }
        }
    }
}