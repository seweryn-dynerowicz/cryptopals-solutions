package set1

import set2.Challenge9
import splitBySize
import java.io.File
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class Challenge7 {
    companion object {
        fun encryptAES(plainBlock : ByteArray, keyBytes: ByteArray) : ByteArray =
            if(plainBlock.size == 16)
                ByteArray(16).apply {
                    val cipher = Cipher.getInstance("AES/ECB/NoPadding").apply {
                        init(Cipher.ENCRYPT_MODE, SecretKeySpec(keyBytes, "AES"))
                    }
                    val cipherBlock = cipher.update(plainBlock)

                    cipherBlock.forEachIndexed { index, byte ->
                        this[index] = byte
                    }
                }
            else error("AES requires 16-bytes block input [got ${plainBlock.size}].")

        fun decryptAES(cipherBlock : ByteArray, keyBytes: ByteArray) : ByteArray =
            if(cipherBlock.size == 16)
                ByteArray(16).apply {
                    val cipher = Cipher.getInstance("AES/ECB/NoPadding").apply {
                        init(Cipher.DECRYPT_MODE, SecretKeySpec(keyBytes, "AES"))
                    }
                    val plainBlock = cipher.update(cipherBlock)

                    plainBlock.forEachIndexed { index, byte ->
                        this[index] = byte
                    }
                }
            else error("AES requires 16-bytes block input [got ${cipherBlock.size}].")

        fun encryptECB(plainBytes: ByteArray, key: ByteArray) : ByteArray {
            val parts = plainBytes.splitBySize(bytesPerParts = 16)
            return ByteArray(16 * parts.size).apply {
                parts.forEachIndexed { index, part ->
                    val plainBlock = Challenge9.pkcs7padding(part, desiredLength = 16)

                    val cipherBlock = encryptAES(plainBlock, key)

                    cipherBlock.forEachIndexed { cbIndex, byte ->
                        this[cbIndex + index * 16] = byte
                    }
                }
            }
        }

        fun decryptECB(cipherBytes: ByteArray, key: ByteArray) : ByteArray =
            ByteArray(cipherBytes.size).apply {
                val parts = cipherBytes.splitBySize(bytesPerParts = 16)
                parts.forEachIndexed { index, cipherBlock ->
                    val clearBlock = Challenge9.pkcs7unpad(decryptAES(cipherBlock, key))

                    clearBlock.forEachIndexed { cbIndex, byte ->
                        this[cbIndex + index*16] = byte
                    }
                }
            }

        @JvmStatic
        fun main(args: Array<String>) {
            val encryptedBase64 = StringBuilder().apply {
                File("challenge7-encrypted.txt").forEachLine { line ->
                    append(line)
                }
            }.toString()

            val keyBytes = "YELLOW SUBMARINE".toByteArray()
            val cipherBytes = Challenge1.fromBase64ToBytes(encryptedBase64)
            val decryptedBytes = decryptECB(cipherBytes, keyBytes)

            println("Decrypted : ${Challenge1.fromBytesToCharacters(decryptedBytes)}")
        }
    }
}