package set1

import kotlin.experimental.and
import kotlin.experimental.or
import kotlin.math.ceil

class Challenge1 {
    companion object {
        private const val BASE64_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

        // Converts an Int value to the Base64 character which represents it.
        // Rejects any value outside the range 0 .. 63
        private fun intToBase64(integer: Int) : Char =
            if(integer in 0 until 64)
                BASE64_CHARACTERS[integer]
            else
                error("Base64 can only encode values in 0 .. 63. Got $integer")

        // Converts a Base64 Character to the Int value which it represents.
        // Rejects any Character that is not valid Base64. Also rejects the padding '='.
        private fun base64ToInt(base64character : Char) : Int =
                when(base64character) {
                    in 'A' .. 'Z' -> base64character - 'A'
                    in 'a' .. 'z' -> base64character - 'a' + 26
                    in '0' .. '9' -> base64character - '0' + 52
                    '+' -> 62
                    '/' -> 63
                    else -> error("Unexpected character for Base64 encoding [$base64character].")
                }

        // Converts a ByteArray to a printable String, only considering printable Characters (i.e. between 32 and 126)
        fun fromBytesToCharacters(bytes: ByteArray) : String =
            StringBuilder().apply {
                bytes.forEach { byte ->
                    if(byte in 32 .. 126 || byte in 9 .. 10)
                        append(byte.toChar())
                }
            }.toString()

        private const val HEXADECIMALS = "0123456789abcdef"

        fun fromByteToHexadecimal(byte: Byte) : String =
            StringBuilder().apply {
                append(HEXADECIMALS[byte.toInt() and 0xf0 ushr 4])
                append(HEXADECIMALS[byte.toInt() and 0x0f])
            }.toString()

        // Converts a ByteArray to a String of hexadecimal Characters (0 .. 9, a .. f) which represent it.
        fun fromBytesToHexadecimals(bytes: ByteArray) : String =
            StringBuilder().apply {
                bytes.forEachIndexed { index, current ->
                    append(HEXADECIMALS[current.toInt() and 0xf0 ushr 4])
                    append(HEXADECIMALS[current.toInt() and 0x0f])
                    if(index < bytes.size - 1)
                        append(' ')
                }
            }.toString()

        // Converts a String of hexadecimal Characters (0 .. 9, a .. f) to the ByteArray it represents
        fun fromHexadecimalsToBytes(hexadecimals: String) : ByteArray =
            ByteArray(ceil(hexadecimals.length / 2f).toInt()).apply {
                hexadecimals.forEachIndexed { nibbleIndex, character ->
                    val nibble = when (character) {
                        in '0' .. '9' -> (character - '0').toByte()
                        in 'A' .. 'F' -> (character - 'A' + 10).toByte()
                        in 'a' .. 'f' -> (character - 'a' + 10).toByte()
                        else -> error("Invalid hexadecimal character")
                    }
                    //setNibble(index, nibble)
                    val byteIndex = nibbleIndex / 2
                    if(nibbleIndex % 2 == 0)
                        // If first nibble is to be set
                        this[byteIndex] = (this[byteIndex] and 0b00001111) or (nibble.toInt() shl 4).toByte()
                    else
                        // If second nibble is to be set
                        this[byteIndex] = (this[byteIndex] and 0b11110000.toByte()) or nibble
                }
            }

        // Converts a group of 4 Base64 Characters to the corresponding group of 3 bytes
        // Result is placed at the proper location in the ByteArray
        private fun convertGroupBase64ToBytes(base64: String, bytes: ByteArray, groupIndex: Int) {
            val indexOfGroupInBase64 = 4 * groupIndex
            val indexOfGroupInBytes  = 3 * groupIndex

            val base64groupValue0 = base64ToInt(base64[indexOfGroupInBase64  ])
            val base64groupValue1 = base64ToInt(base64[indexOfGroupInBase64+1])

            // Byte 0
            val byte0 = (base64groupValue0 and 0b00111111  shl 2) or
                        (base64groupValue1 and 0b00110000 ushr 4)
            bytes[indexOfGroupInBytes] = byte0.toByte()

            // Byte 1
            if(indexOfGroupInBase64+2 < base64.length && base64[indexOfGroupInBase64+2] != '=') {
                val base64groupValue2 = base64ToInt(base64[indexOfGroupInBase64+2])
                val byte1 = (base64groupValue1 and 0b00001111 shl 4) or
                            (base64groupValue2 and 0b00111100 ushr 2)
                bytes[indexOfGroupInBytes + 1] = byte1.toByte()

                // Byte 2
                if(indexOfGroupInBase64+3 < base64.length && base64[indexOfGroupInBase64+3] != '=') {
                    val base64groupValue3 = base64ToInt(base64[indexOfGroupInBase64+3])
                    val byte2 = (base64groupValue2 and 0b00000011 shl 6) or
                                (base64groupValue3 and 0b00111111)
                    bytes[indexOfGroupInBytes + 2] = byte2.toByte()
                }
            }

        }

        // Converts a String of Base64 Characters to the ByteArray it encodes.
        // Will reject a String not properly padded with '=' (i.e. length % 4 != 0)
        fun fromBase64ToBytes(base64: String) : ByteArray {
            // A Base64 String must contain a number of Characters that is a multiple of 4
            if(base64.length % 4 != 0)
                error("String is not properly padded.")
            // The units considered throughout are the groups of 4 Base64 Characters
            val numberOfBase64groups = base64.length / 4

            // Each group of 4 Base64 Characters encodes a group of 3 bytes
            // (n.b. the preliminary number of Bytes is thus 3/4 of the string length)
            var byteCount = 3 * numberOfBase64groups
            if(base64[base64.length - 1] == '=') {
                // If the last Base64 Character is an '=' (i.e. padding symbol), the last group of 4 Base64 Characters
                // only encodes 2 significant bytes
                byteCount--
                if (base64[base64.length - 2] == '=')
                    // If the penultimate Base64 Character is also an '=', the last group of 4 Base64 Characters
                    // only encodes 1 significant byte
                    byteCount--
            }
            // Thus the number of significant bytes encoded by the Base64String is equal to 3 * base64.length/4
            // minus 1 byte if the last Character is '='
            // minus 1 additional byte if the penultimate Character is also '='
            val bytes = ByteArray(byteCount)

            // We must go over each group of 4 Base64 Characters to produce the corresponding group of 3 bytes
            for(groupeIndex in 0 until numberOfBase64groups)
                convertGroupBase64ToBytes(base64, bytes, groupeIndex)

            return bytes
        }

        // Converts a group of 3 bytes to the corresponding group of 4 Base64 Characters4
        // Result is appended to the StringBuilder
        private fun convertGroupBytesToBase64(bytes: ByteArray, base64: StringBuilder, groupIndex: Int) {
            val indexOfGroupInBytes  = 3 * groupIndex

            val byte0 = bytes[indexOfGroupInBytes].toInt()
            val byte1 = if(indexOfGroupInBytes+1 < bytes.size) bytes[indexOfGroupInBytes+1].toInt() else 0
            val byte2 = if(indexOfGroupInBytes+2 < bytes.size) bytes[indexOfGroupInBytes+2].toInt() else 0

            // Base64 Character 0
            val bytesGroupValue0 = byte0 and 0b11111100 ushr 2
            base64.append(intToBase64(bytesGroupValue0))

            // Base64 Character 1
            val bytesGroupValue1 =
                    (byte0 and 0b00000011  shl 4) or
                    (byte1 and 0b11110000 ushr 4)
            base64.append(intToBase64(bytesGroupValue1))

            // Base64 Character 2
            if(indexOfGroupInBytes+1 < bytes.size) {
                val bytesGroupValue2 =
                        (byte1 and 0b00001111  shl 2) or
                        (byte2 and 0b11000000 ushr 6)
                base64.append(intToBase64(bytesGroupValue2))
            } else {
                base64.append('=')
            }

            // Base64 Character 3
            if(indexOfGroupInBytes+2 < bytes.size) {
                val bytesGroupValue3 = byte2 and 0b00111111
                base64.append(intToBase64(bytesGroupValue3))
            } else {
                base64.append('=')
            }
        }

        // Challenge 1 : https://cryptopals.com/sets/1/challenges/1
        fun fromBytesToBase64(bytes: ByteArray) : String =
                StringBuilder().apply {
                    val numberOfBytesGroups = ceil(bytes.size.toFloat() / 3).toInt()

                    // We must go over each group of 4 Base64 Characters to produce the corresponding group of 3 bytes
                    for(groupeIndex in 0 until numberOfBytesGroups)
                        convertGroupBytesToBase64(bytes , this, groupeIndex)

                }.toString()

        @JvmStatic
        fun main(args: Array<String>) {
            val input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
            // Expected : SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t

            val bytes = fromHexadecimalsToBytes(hexadecimals = input)

            println("Input : $input")
            // Go over each group of three bytes to produce four base64 characters
            println("Output : ${fromBytesToBase64(bytes)}")
            
            val input2 = "Man is distinguished, not only by his reason, but by this singular passion from other animals, " +
                    "which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable " +
                    "generation of knowledge, exceeds the short vehemence of any carnal pleasure."
            println("Input 2 : ${input2.length}")
            println("Output 2 : ${fromBytesToBase64(input2.toByteArray())}")

            val test1 = "Ahoi, matey !"

            println("Encode : $test1")
            val hexade = fromBytesToHexadecimals(test1.toByteArray())
            println("Hexade : $hexade")
            val base64 = fromBytesToBase64(test1.toByteArray())
            println("Base64 (NEW) : $base64")
            //println("Base64 (OLD) : ${oldFromBytesToBase64(test1.toByteArray())}")
            val bytes2 = fromBase64ToBytes(base64)
            println("Hexad2 : ${fromBytesToHexadecimals(bytes2)}")
            println("Decode : ${fromBytesToCharacters(bytes2)}")
        }
    }
}