package set1

import xor

// Challenge 2 : https://cryptopals.com/sets/1/challenges/2
class Challenge2 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val hexadecimals1 = "1c0111001f010100061a024b53535009181c"
            val hexadecimals2 = "686974207468652062756c6c277320657965"

            val bytes1 = Challenge1.fromHexadecimalsToBytes(hexadecimals1)
            val bytes2 = Challenge1.fromHexadecimalsToBytes(hexadecimals2)

            val fixedXOR = bytes1 xor bytes2
            println("Outcome : ${Challenge1.fromBytesToHexadecimals(fixedXOR)}")
        }
    }
}

