package set1

import java.io.File

class Challenge4 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val file = File("challenge4-encrypteds.txt")

            var lineIndex = 0

            var minimalScore = Float.MAX_VALUE
            var matchingKey = 0.toByte()
            var matchingLine = 0
            var matchingBytes = ByteArray(0)

            file.forEachLine { line ->
                val bytes = Challenge1.fromHexadecimalsToBytes(line)
                val (key, score) =  Challenge3.minimalScore(bytes)
                if(score < minimalScore) {
                    minimalScore = score
                    matchingLine = lineIndex
                    matchingBytes = bytes
                    matchingKey = key
                }
                lineIndex++
            }

            println("Best candidate : line $matchingLine with key '${matchingKey.toChar()}' has score $minimalScore")
            val decoded = Challenge3.singleByteXOR(matchingBytes, matchingKey)
            println("> ${Challenge1.fromBytesToCharacters(decoded)}")

        }
    }
}