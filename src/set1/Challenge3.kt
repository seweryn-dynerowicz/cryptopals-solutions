package set1

import kotlin.experimental.xor
import kotlin.math.abs

// Challenge 3 : https://cryptopals.com/sets/1/challenges/3
class Challenge3 {
    companion object {
        // Frequency of letters in the English language based on texts
        // Taken from https://en.wikipedia.org/wiki/Letter_frequency
        private val LETTER_FREQUENCIES = floatArrayOf(
            0.08167f, 0.01492f, 0.02782f, 0.04253f,
            0.12702f, 0.02228f, 0.02015f, 0.06094f,
            0.06966f, 0.00153f, 0.00772f, 0.04025f,
            0.02406f, 0.06749f, 0.07507f, 0.01929f,
            0.00095f, 0.05987f, 0.06327f, 0.09056f,
            0.02758f, 0.00978f, 0.02360f, 0.00150f,
            0.01974f, 0.00074f
        )

        fun singleByteXOR(input: ByteArray, key: Byte) : ByteArray =
            ByteArray(input.size).apply {
                input.forEachIndexed { index, byte ->
                    this[index] = byte.xor(key)
                }
            }

        fun countLetters(input: ByteArray) : Float {
            var count = 0f
            input.forEach { byte ->
                if(byte in 65 .. 90 || byte in 97 .. 122)
                    count += 1f
            }
            return count
        }

        fun frequencyCount(input: ByteArray) : FloatArray =
            FloatArray(26).apply {
                input.forEach { byte ->
                    when(val code = byte.toInt()) {
                        // Ignore all printable characters which are not letters or LETTERS
                        in 65 .. 90  -> this[code - 65] += 1f
                        in 97 .. 122 -> this[code - 97] += 1f
                    }
                }
                // Normalize frequencies
                forEachIndexed { index, count ->
                    this[index] = (count / countLetters(input))
                }
            }

        fun scoreBytes(bytes: ByteArray) : Float {
            var score = 0.0f
            val histogram = frequencyCount(bytes)
            for(index in 0 until 26) {
                score += abs(LETTER_FREQUENCIES[index] - histogram[index])
            }
            return score
        }

        fun validCharacters(bytes: ByteArray) : Boolean =
            bytes.all { byte ->
                val code = byte.toInt()
                code in 9 .. 10 || code in 13 .. 13 || code in 32 .. 126
            }

        fun minimalScore(bytes: ByteArray) : Pair<Byte, Float> {
            var keyFound = 0.toByte()
            var minimalScore = Float.MAX_VALUE

            for(key in 32.toByte() until 126.toByte()) {
                val decoded = singleByteXOR(bytes, key.toByte())
                // Only score texts which contains no unprintable ASCII character
                if(validCharacters(decoded)) {
                    val currentScore = scoreBytes(decoded)
                    if(currentScore < minimalScore) {
                        keyFound = key.toByte()
                        minimalScore = currentScore
                    }
                }
            }

            return Pair(keyFound, minimalScore)
        }

        fun allScores(bytes: ByteArray) : Map<Byte, Float> =
            HashMap<Byte, Float>().apply {
                for(key in 32.toByte() until 126.toByte()) {
                    val decoded = singleByteXOR(bytes, key.toByte())
                    if(validCharacters(decoded))
                        this[key.toByte()] = scoreBytes(decoded)
                }
            }.toList().sortedBy { (_, score) -> score }.toMap()

        fun displayScores(bytes: ByteArray, count: Int) {
            val scores = allScores(bytes)
            var index = 0
            scores.forEach { (key, score) ->
                if(index < count) {
                    val decoded = singleByteXOR(bytes, key)
                    println(" > Key ${key.toChar()} ($score) : ${Challenge1.fromBytesToCharacters(decoded)}")
                }
                index++
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

            val bytes = Challenge1.fromHexadecimalsToBytes(input)

            displayScores(bytes, count = 3)
        }
    }
}