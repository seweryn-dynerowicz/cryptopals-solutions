package set1

import splitBySize
import java.io.File

class Challenge8 {
    companion object {
        fun isEncryptedWithECB(line: String) : Boolean {
            val partsSummary = HashSet<String>().apply {
                addAll(line.splitBySize(sizeOfParts = 32))
            }
            return partsSummary.size != 10
        }

        fun isEncryptedWithECB(bytes: ByteArray) : Boolean {
            if(bytes.size % 16 != 0)
                error("Improperly sized ByteArray. Not a multiple of 16.")
            val partsSummary = HashSet<String>().apply {
                bytes.splitBySize(bytesPerParts = 16).forEach { part ->
                    add(Challenge1.fromBytesToHexadecimals(part))
                }
            }
            return partsSummary.size != (bytes.size / 16)
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val file = File("challenge8-encrypteds.txt")

            var index = 0
            file.forEachLine { line ->
                if(isEncryptedWithECB(line)) {
                    println("Line $index contains repeating blocks of 16 bytes")
                    println("> $line")
                }
                index++
            }
        }
    }
}