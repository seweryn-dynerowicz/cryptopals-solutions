import kotlin.experimental.xor
import kotlin.math.ceil

infix fun ByteArray.xor(other: ByteArray) : ByteArray =
        ByteArray(size).apply {
            if(this@xor.size != other.size)
                error("XOR requires equal-length operands")

            for(index in 0 until size)
                this[index] = this@xor[index].xor(other[index])
        }

fun String.splitBySize(sizeOfParts: Int) : List<String> =
    ArrayList<String>().apply {
        if(this@splitBySize.length % sizeOfParts != 0)
            error("Cannot split into a round number of parts")

        val numberOfParts = this@splitBySize.length / sizeOfParts
        for(partIndex in 0 until numberOfParts) {
            var substring = ""
            for(index in 0 until sizeOfParts) {
                substring += this@splitBySize[partIndex * sizeOfParts + index]
            }
            add(substring)
        }
    }

fun ByteArray.subArray(range: IntRange) : ByteArray =
    ByteArray(range.count()).apply {
        var index = 0
        for(tIndex in range) {
            this[index] = this@subArray[tIndex]
            index += 1
        }
    }

// Splits this ByteArray into parts of size <bytesPerParts> padded as necessary.
fun ByteArray.splitBySize(bytesPerParts: Int) : List<ByteArray> =
        ArrayList<ByteArray>().apply {
            val numberOfParts = ceil(this@splitBySize.size.toFloat() / bytesPerParts).toInt()
            for(partIndex in 0 until numberOfParts) {
                val subarray = ByteArray(bytesPerParts, init = { 0x04 })
                for(index in subarray.indices) {
                    val originalIndex = partIndex * bytesPerParts + index
                    if(originalIndex < this@splitBySize.size)
                        subarray[index] = this@splitBySize[originalIndex]
                }
                add(subarray)
            }
        }

fun ByteArray.split(numberOfParts: Int) : List<ByteArray> =
    ArrayList<ByteArray>().apply {
        val partSize = ceil(this@split.size.toDouble() / numberOfParts).toInt()
        val numberOfFullParts = this@split.size % numberOfParts
        for(partIndex in 0 until numberOfParts) {
            val currentPartSize = if(partIndex in 0 until numberOfFullParts) partSize else partSize - 1
            val part = ByteArray(currentPartSize)
            for(index in 0 until currentPartSize)
                part[index] = this@split[index * numberOfParts + partIndex]
            add(part)
        }
    }