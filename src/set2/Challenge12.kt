package set2

import set1.Challenge1
import set1.Challenge7
import splitBySize
import subArray
import kotlin.random.Random

class Challenge12 {
    companion object {
        private val unknownKey : ByteArray = Random.nextBytes(16)
        private val unknownString : ByteArray = Challenge1.fromBase64ToBytes(
      "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
        )
        private val unknownPrefix = "hahahohohihihuhu".toByteArray()

        fun encryption(input: String) : ByteArray =
            Challenge7.encryptECB(plainBytes = input.toByteArray() + unknownString, key = unknownKey)

        // The block size can be figured out by attempting the encryption() over increasingly long prefixes
        // A, AA, AAA,  AAAA, AAAAA, ...
        // At some point in this loop, the first block of the output will stop changing because in the two
        // successive iterations, the first block of the input will be identical.
        fun detectBlockSize() : Int {
            var count = 1
            var previousFirstBlock = Challenge1.fromBytesToHexadecimals(encryption("").subArray(0 until 16))
            while(count < 64) {
                val firstBlock = Challenge1.fromBytesToHexadecimals(encryption("A".repeat(count)).subArray(0 until 16))
                if(firstBlock  == previousFirstBlock)
                    return count - 1
                previousFirstBlock = firstBlock
                count += 1
            }
            return -1
        }

        fun isEncryptedWithECB() : Boolean {
            val cipherBytes = encryption("0123456789abcdef0123456789abcdef")
            val firstBlock = Challenge1.fromBytesToHexadecimals(cipherBytes.copyOfRange(0, 16))
            val secondBlock = Challenge1.fromBytesToHexadecimals(cipherBytes.copyOfRange(16, 32))
            return firstBlock == secondBlock
        }

        val LETTERS = StringBuilder().apply {
            for(code in 0 until 256)
                append(code.toChar())
        }.toString()

        // Make a dictionary of encrypting <prefix ++ Char> for every possible Char.
        // The output is a string of hexadecimal characters
        fun makeDictionary(prefix: String) : Map<String, Char> =
            HashMap<String, Char>().apply {
                LETTERS.forEach { letter ->
                    val input = prefix.toByteArray() + letter.toByte()
                    val cipherBytes = Challenge7.encryptAES(input, unknownKey)
                    this[Challenge1.fromBytesToHexadecimals(cipherBytes)] = letter
                }
            }

        fun decryptByteByByte(blockSize: Int) {
            var previousBlock = "A".repeat(blockSize)
            for(blockIndex in 0 until 9) {
                var decipheredText = ""
                for (index in 0 until 16) {
                    val prefix = previousBlock.subSequence(index + 1, blockSize).toString()
                    val dictionary = makeDictionary(prefix + decipheredText)
                    val targetBlock = encryption(prefix).splitBySize(16)[blockIndex]
                    val lastByte = dictionary[Challenge1.fromBytesToHexadecimals(targetBlock)] ?: error("Broken deciphering")
                    decipheredText += lastByte
                }
                val unpadded = Challenge9.pkcs7unpad(decipheredText.toByteArray())
                print(Challenge1.fromBytesToCharacters(unpadded))
                previousBlock = decipheredText
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val blockSize = detectBlockSize()
            val isEncryptedWithECB = isEncryptedWithECB()
            println("Guessed block size : $blockSize bytes")
            println("Was AES-ECB used   ? $isEncryptedWithECB")
            println("\nDecrypted byte-byte :")
            decryptByteByByte(blockSize)
        }
    }
}