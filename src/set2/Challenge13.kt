package set2

import java.lang.StringBuilder
import kotlin.math.absoluteValue
import kotlin.random.Random

class Challenge13 {
    companion object {
        fun parseKV(string: String) : Map<String, String> =
            HashMap<String, String>().apply {
                string.split("&").forEach { kv ->
                    val keyvalue = kv.split('=')
                    val key = keyvalue[0]
                    val value = keyvalue[1]
                    this[key] = value
                }
            }

        fun profile(email: String) : String =
            StringBuilder().apply {
                append("email=")
                append(email)
                append('&')
                append("uid=")
                append(Random.nextInt().absoluteValue)
                append('&')
                append("role=user")
            }.toString()

        @JvmStatic
        fun main(args: Array<String>) {
            val scookie = profile("seweryn.dynerowicz@gmail.com")
            parseKV(scookie).forEach { (key, value) ->
                println("> $key=$value")
            }
        }
    }
}