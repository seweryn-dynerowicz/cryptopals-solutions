package set2

import set1.Challenge1

class Challenge9 {
    // Challenge 9 : https://cryptopals.com/sets/2/challenges/9

    companion object {
        fun pkcs7padding(bytes: ByteArray, desiredLength: Int) : ByteArray =
            ByteArray(desiredLength).apply {

                bytes.forEachIndexed { index, byte -> this[index] = byte }

                val padByte = (desiredLength - bytes.size).toByte()

                for (index in bytes.size until desiredLength)
                    this[index] = padByte
            }

        private fun detectPkcs7pad(bytes: ByteArray) : Int {
            val lastByte = bytes.last()
            if(lastByte in bytes.indices) {
                var index = bytes.size - lastByte
                while (index < bytes.size)
                    if (bytes[index] == lastByte)
                        index++
                    else
                        return 0
                return lastByte.toInt()
            }
            else return 0
        }

        fun pkcs7unpad(bytes: ByteArray) : ByteArray =
            ByteArray(bytes.size - detectPkcs7pad(bytes), init = { index -> bytes[index] })

        @JvmStatic
        fun main(args: Array<String>) {
            val input = "YELLOW SUBMARINE"
            val padded = pkcs7padding(input.toByteArray(), desiredLength = 20)
            println("Padded      : ${Challenge1.fromBytesToCharacters(padded)}")
            println("Hexadecimal : ${Challenge1.fromBytesToHexadecimals(padded)}")

            println("Detected padding : ${detectPkcs7pad(padded)}")
            println("Unpadded    : ${Challenge1.fromBytesToHexadecimals(pkcs7unpad(padded))}")
        }
    }
}