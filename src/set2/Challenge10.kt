package set2

import set1.Challenge1
import set1.Challenge7
import splitBySize
import xor
import java.io.File

class Challenge10 {
    // Challenge 10 : https://cryptopals.com/sets/2/challenges/10

    companion object {
        fun encryptCBC(plainBytes: ByteArray, key: ByteArray, iv: ByteArray = ByteArray(key.size)) : ByteArray {
            val parts = plainBytes.splitBySize(bytesPerParts = 16)
            return ByteArray(parts.size * 16).apply {
                var previousCipher = iv
                parts.forEachIndexed { index, part ->
                    val clearBlock = Challenge9.pkcs7padding(part, desiredLength = 16)

                    val inputBlock = clearBlock.xor(previousCipher)
                    val cipherBlock = Challenge7.encryptAES(inputBlock, key)

                    previousCipher = cipherBlock

                    cipherBlock.forEachIndexed { cbIndex, byte ->
                        this[cbIndex + index * 16] = byte
                    }
                }
            }
        }

        fun decryptCBC(cipherBytes: ByteArray, key: ByteArray, iv: ByteArray = ByteArray(key.size)) : ByteArray =
                ByteArray(cipherBytes.size).apply {
                    val blocks = cipherBytes.splitBySize(bytesPerParts = 16)
                    var previousCipher = iv
                    blocks.forEachIndexed { index, cipherBlock ->
                        val outputBlock = Challenge9.pkcs7unpad(Challenge7.decryptAES(cipherBlock, key))
                        val clearBlock = outputBlock.xor(previousCipher)
                        previousCipher = cipherBlock

                        clearBlock.forEachIndexed { cbIndex, byte ->
                            this[cbIndex + index*16] = byte
                        }
                    }
                }

        @JvmStatic
        fun main(args: Array<String>) {
            val encryptedBase64 = StringBuilder().apply {
                File("challenge10-encrypted.txt").forEachLine { line ->
                    append(line)
                }
            }.toString()

            val keyBytes = "YELLOW SUBMARINE".toByteArray()
            val cipherBytes = Challenge1.fromBase64ToBytes(encryptedBase64)

            val clearBytes = decryptCBC(cipherBytes, keyBytes)

            println("Clear text : ${Challenge1.fromBytesToCharacters(clearBytes)}")
        }
    }
}