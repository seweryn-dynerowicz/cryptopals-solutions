package set2

import set1.Challenge1
import set1.Challenge7
import splitBySize
import subArray
import kotlin.random.Random

class Challenge14 {
    companion object {
        private val key : ByteArray = Random.nextBytes(16)
        private val prefixSize = Random.nextInt(2, 12)
        private val prefix: ByteArray = ByteArray(prefixSize, init = {
            Random.nextInt(32, 126).toByte()
            //Random.nextBytes(prefixSize)
        })
        private val target : ByteArray = Challenge1.fromBase64ToBytes(
        "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
        )

        fun encryption(control: String) : ByteArray =
            Challenge7.encryptECB(plainBytes = prefix + control.toByteArray() + target, key = key)

        // Only detects prefix assumed to have less than 16 bytes.
        // Maybe not in all cases. Need to think more about this.
        // Can be extended to deal with longer prefixes.
        fun detectPrefixSize() : Int {
            var count = 1
            var previousFirstBlock = Challenge1.fromBytesToHexadecimals(encryption("").subArray(0 until 16))
            println("First block (count=0) : $previousFirstBlock")
            while(count < 64) {
                val controlPad = "=".repeat(count)
                val firstBlock = Challenge1.fromBytesToHexadecimals(encryption(controlPad).subArray(0 until 16))
                println("First block (count=$count) : $firstBlock")
                if(firstBlock  == previousFirstBlock) {
                    //println("Stable block (${count-1}) : $firstBlock")
                    return 16 - (count - 1)
                }
                previousFirstBlock = firstBlock
                count += 1
            }
            return -1
        }

        fun decryptByteByByte(blockSize: Int, prefixComplementSize: Int) {
            var previousBlock = "A".repeat(blockSize)
            val controlPrefix = "=".repeat(prefixComplementSize)
            for(blockIndex in 0 until 9) {
                var decipheredText = ""
                for (index in 0 until 16) {
                    val control = previousBlock.subSequence(index + 1, blockSize).toString()
                    val dictionary = Challenge12.makeDictionary(control + decipheredText)
                    println("Current control ($blockIndex) : ${controlPrefix + control}")
                    val blocks = encryption(controlPrefix + control).splitBySize(16)
                    println("Block0 (${prefix.size + controlPrefix.length}) : ${Challenge1.fromBytesToHexadecimals(blocks[0])}")
                    println("Block1 (${control.length}) : ${Challenge1.fromBytesToHexadecimals(blocks[1])}")
                    val targetBlock = blocks[blockIndex+1]
                    println("Target : ${Challenge1.fromBytesToHexadecimals(targetBlock)}")
                    val lastByte = dictionary[Challenge1.fromBytesToHexadecimals(targetBlock)] ?: error("Broken deciphering")
                    decipheredText += lastByte
                }
                print(decipheredText)
                previousBlock = decipheredText
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            println(Challenge12.LETTERS)
            val prefixSize = detectPrefixSize()
            println("Prefix size ? $prefixSize ${this.prefixSize}")

            val neededPrefixComplement = 16 - prefixSize
            println("Prefix complement : $neededPrefixComplement")

            decryptByteByByte(16, neededPrefixComplement)
        }
    }
}