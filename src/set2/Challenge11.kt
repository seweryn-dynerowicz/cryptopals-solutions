package set2

import set1.Challenge7
import set1.Challenge8
import java.io.File
import kotlin.random.Random

class Challenge11 {
    companion object {
        fun randomizeEncryption(input: String) : ByteArray {
            val prefixSize = Random.nextInt(from = 5, until = 11)
            val suffixSize = Random.nextInt(from = 5, until = 11)
            val clearBytes = Random.nextBytes(prefixSize) + input.toByteArray() + Random.nextBytes(suffixSize)
            val useCBC = Random.nextBoolean()
            println("Input : ${input.length}")
            println("> prefix:$prefixSize, suffix:$suffixSize, encrypt with ${if(useCBC) "CBC" else "ECB"}")
            return if(useCBC)
                Challenge10.encryptCBC(plainBytes = clearBytes, key = Random.nextBytes(16), iv = Random.nextBytes(16))
            else
                Challenge7.encryptECB(plainBytes = clearBytes, key = Random.nextBytes(16))
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val input = StringBuilder().apply {
                /* No matter how many bytes of prefix (between 5 and 10)
                   are added, there will always be two identical blocks
                   of 16 bytes in the resulting input.
                   If the prefix is  5 bytes, then block =====ABCDEFGHIJK will occur twice
                                     6 bytes,            ====ABCDEFGHIJKL
                                     7 bytes,            ===ABCDEFGHIJKLM
                                     8 bytes,            ==ABCDEFGHIJKLMN
                                     9 bytes,            =ABCDEFGHIJKLMNO
                                    10 bytes,            ABCDEFGHIJKLMNOP
                 */
                File("challenge11-crafty-input.txt").forEachLine { line ->
                    append(line)
                }
            }.toString()

            for(attempt in 0 until 10) {
                val cipherBytes = randomizeEncryption(input)
                val isEncryptedWithECB = Challenge8.isEncryptedWithECB(cipherBytes)
                println("> Oracle ? This was encrypted using ${if(isEncryptedWithECB) "ECB" else "CBC"}")
            }
        }
    }
}